import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
 
@Injectable({ providedIn: 'root' })
export class TokenService {

  public TOKEN_KEY = "JWTGMPTOKEN";
 
  constructor() { }

  public decodeExpToken(token: string): any {
    if(token===null) {
      return null;
    }

    let parts = token.split('.');

    if (parts.length !== 3) {
      throw new Error('The inspected token doesn\'t appear to be a JWT. Check to make sure it has three parts and see https://jwt.io for more.');
    }

    var splitToken = token.split('.')[1];
    var base64 = splitToken.replace('-', '+').replace('_', '/');
    var decoded = JSON.parse(window.atob(base64))

    if (!window.atob(base64)) {
      throw new Error('Cannot decode the token.');
    }

    return decoded;
  }

  public getTokenExpirationDate(token: string): Date | null {
    let decoded: any;
    decoded = this.decodeExpToken(token);

    if (!decoded.hasOwnProperty('exp')) {
      return null;
    }

    const date = new Date(0);
    date.setUTCSeconds(decoded.exp);

    return date;
  }

  public isTokenExpired(token: string, offsetSeconds?: number): boolean {
    if (token === null || token === '') {
        return true;
    }
    let date = this.getTokenExpirationDate(token);
    offsetSeconds = offsetSeconds || 0;

    if (date === null) {
      return true;
    }

    return !(date.valueOf() > new Date().valueOf() + offsetSeconds * 1000);
  }

  public publicHeader() {
    let jwt = {
      headers: new HttpHeaders({ 
        'Content-Type': 'application/json'
      })
    };

    return jwt;
  }

  public jwtBearer() {
    let jwt = {
      headers: new HttpHeaders({ 
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem(this.TOKEN_KEY) 
      })
    };

    return jwt;
  }

  public fileBearer() {
    let jwt = {
      headers: new HttpHeaders({ 
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem(this.TOKEN_KEY) 
      })
    };
    return jwt;
  }

  public blobMixBearer() {
    let jwt = {
      headers: new HttpHeaders({ 
        'Authorization': 'Bearer ' + localStorage.getItem(this.TOKEN_KEY) 
      })
    };

    return jwt;
  }

  public imageBearer() {
    let jwt = {
      headers: new HttpHeaders({ 
        'Authorization': 'Bearer ' + localStorage.getItem(this.TOKEN_KEY),
        // 'Content-Type': 'image/png',
        // observe: 'response',
        
      }),
      // responseType: 'text' as 'json'
      responseType: 'blob' as 'json',
      // observe: 'response' as 'body'
    };

    return jwt;
  }

  public blobBearer() {
    let jwt = {
      headers: new HttpHeaders({ 
        responseType: 'blob' as 'json', 
        observe: 'response' as 'body'
      })
    };
    return jwt;
  }

  public jwtFile() {
    let token = localStorage.getItem(this.TOKEN_KEY)
    let jwtFile = {
      headers: new HttpHeaders({ 
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };

    return jwtFile;
  }
  
}