import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { ErrorHandlerService } from './util/error.handler.service';


const API_URL = 'https://kc-metronic-api.fotia.com.my';

@Injectable({
    providedIn: 'root'
})
export class KCFetchService {

    constructor(
        private http: HttpClient,
        private _error: ErrorHandlerService,
      ) { }
    getjwt (): Observable<any> {
        return this.http.get(API_URL+"/api/jwt/data").pipe(
            tap(),
            catchError(this._error.handleError<any>('Fetch JWT Metronic API'))
        );
      }

      getpublic (): Observable<any> {
        return this.http.get(API_URL+"/api/public/data").pipe(
            tap(),
            catchError(this._error.handleError<any>('Fetch Public Metronic API'))
        );
      }
      
}