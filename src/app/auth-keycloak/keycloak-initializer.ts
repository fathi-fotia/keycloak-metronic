import { KeycloakOptions, KeycloakService } from 'keycloak-angular';
import { environment } from '../../environments/environment';
import { from } from 'rxjs';

export function initializer(keycloak: KeycloakService): () => Promise<boolean> {


    const options: KeycloakOptions = {
      config : environment.keycloak,
      loadUserProfileAtStartUp: true,
      initOptions: {
          onLoad: 'check-sso',
          // onLoad: 'login-required',
          checkLoginIframe: false
      },
      bearerExcludedUrls: ['/api/public/*']
    };

    return async () => {
      from(keycloak.keycloakEvents$).subscribe(event => console.log("event: ", event))

    return keycloak.init(options)
    }
}