import { KeycloakService } from 'keycloak-angular';
// Angular
import { Component, OnInit } from '@angular/core';
import { KCFetchService } from '../../../services/kc-fetch.service';
import { NgxPermissionsService } from 'ngx-permissions';

@Component({
  selector: 'kt-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {

  constructor(
    private _kcFetchService: KCFetchService,
    private _kcService: KeycloakService,
    permissionsService: NgxPermissionsService
  ) {
    permissionsService.loadPermissions(this._kcService.getUserRoles());
  }

  ngOnInit(): void {
    this._kcService.loadUserProfile().then((profile) => {
      console.log(profile);
    })
    console.log(JSON.stringify(this._kcService.getKeycloakInstance().tokenParsed));
    console.log(JSON.parse(JSON.stringify(this._kcService.getKeycloakInstance().tokenParsed)));
    console.log(JSON.stringify(this._kcService.getKeycloakInstance().userInfo));
    this._kcService.getToken().then((token)=>{
      console.log('TOKEN: ' + token)
    })
    console.log('User Roles: ' + this._kcService.getUserRoles())
    
    this._kcFetchService.getpublic().subscribe(
      success=>{
         console.log(success)
      },
      error =>{
        console.log(error)
      }
    )

    this._kcFetchService.getjwt().subscribe(
      success=>{
         console.log(success)
      },
      error =>{
        console.log(error)
      }
    )
  }
}
