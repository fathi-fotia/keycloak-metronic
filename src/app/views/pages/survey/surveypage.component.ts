import { Component, OnInit } from '@angular/core';
import json from "../../../../assets/survey.json";

@Component({
  selector: 'kt-surveypage',
  templateUrl: './surveypage.component.html',
  // styleUrls: ['./survey.component.scss']
})
export class SurveyPageComponent implements OnInit {

  json= json;
  constructor() { }

  ngOnInit(): void {
  }

  onSurveySaved(survey){
    this.json = survey;
  }

}
