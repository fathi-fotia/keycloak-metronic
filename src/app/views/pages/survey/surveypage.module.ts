import { SurveyCreatorComponent } from './../../../components/survey.creator.component';
import { SurveyComponent } from './../../../components/survey.component';
import { CoreModule } from '../../../core/core.module';
// Angular
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgbDropdownModule, NgbTabsetModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { PartialsModule } from '../../partials/partials.module';
import { SurveyPageComponent } from './surveypage.component';

@NgModule({
    imports: [
      CommonModule,
      PartialsModule,
      CoreModule,
      RouterModule.forChild([
        {
          path: '',
          component: SurveyPageComponent
        },
      ]),
      // ng-bootstrap modules
      NgbDropdownModule,
      NgbTabsetModule,
      NgbTooltipModule,
    ],
    providers: [],
    declarations: [
        SurveyPageComponent,SurveyComponent, SurveyCreatorComponent
    ]
  })
  export class SurveyPageModule {
  }