import { KeycloakConfig } from 'keycloak-js';

const keycloakConfig: KeycloakConfig = {
  url: 'https://keycloak.fotia.com.my:8443/auth',
  realm: 'demo',
  clientId: 'metronic-angular',
};

export default keycloakConfig;